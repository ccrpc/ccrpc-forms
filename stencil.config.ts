import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'ccrpc-forms',
  globalStyle: 'src/global/ccrpc-forms.scss',
  buildEs5: 'prod',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
      copy: [
        {
          src: '../node_modules/formiojs/dist/fonts',
          dest: 'fonts'
        }
      ]
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null,
      copy: [
        {
          src: '../node_modules/formiojs/dist/fonts',
          dest: 'build/fonts'
        }
      ]
    },
  ],
  plugins: [
    sass()
  ]
};
