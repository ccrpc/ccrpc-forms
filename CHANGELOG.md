# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/ccrpc/ccrpc-forms/compare/v0.0.1...v0.0.2) (2021-06-17)


### Bug Fixes

* move font-face to global CSS ([3a2c18f](https://gitlab.com/ccrpc/ccrpc-forms/commit/3a2c18fdc19f718803c493b21f2dc7a4b1d744c1)), closes [#1](https://gitlab.com/ccrpc/ccrpc-forms/issues/1)

### 0.0.1 (2021-05-28)


### Bug Fixes

* **examples:** load bundles using relative URL ([2e7dbc0](https://gitlab.com/ccrpc/ccrpc-forms/commit/2e7dbc0b42e98e7f3dda4bfa9a6cf04232442445))
