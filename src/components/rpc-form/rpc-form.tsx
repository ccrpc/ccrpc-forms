import { Component, Prop, h } from '@stencil/core';
import { Formio } from 'formiojs';

@Component({
  tag: 'rpc-form',
  styleUrl: 'rpc-form.scss',
  shadow: false
})
export class Form {
  formEl: HTMLElement;
  /**
   * API URL of the form
   */
  @Prop() url: string;

  async componentDidLoad() {
    Formio.createForm(this.formEl, this.url);
  }

  render() {
    return (
      <div class="formio" ref={(el) => this.formEl = el}></div>
    );
  }
}
