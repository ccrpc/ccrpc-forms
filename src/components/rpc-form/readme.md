# rpc-form

The form component takes the API URL of a Form.io form and renders the form on the page.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description         | Type     | Default     |
| -------- | --------- | ------------------- | -------- | ----------- |
| `url`    | `url`     | API URL of the form | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
