function replaceAttrs(text, replace) {
  for (var prop in replace) {
    var propHyphen =
      prop.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
    var value = replace[prop];
    text = text.replace(
      new RegExp(propHyphen + '=".+"'),
      propHyphen + '="' + value.replace(/\"/g, '&quot;') + '"');
  }
  return text;
}
function createCode(el, replace) {
  var pre = document.createElement('pre');
  var code = document.createElement('code');
  code.classList.add('html');
  code.textContent = el.outerHTML
    .replace(/" /g, "\"\n   ")
    .replace('assets/', '');
  if (replace) code.textContent = replaceAttrs(code.textContent, replace);
  pre.appendChild(code);
  el.parentNode.insertBefore(pre, el);
  hljs.highlightBlock(code);
}
function destroyCode(el) {
  if (el.nextSibling.tagName === 'PRE') el.nextSibling.remove();
}
function updateComponent(e, valueProp, textProp) {
  var select = e.target;
  var el = select.nextElementSibling;
  var replace = {};
  replace[valueProp] = el[valueProp] = select.value;
  replace[textProp] = el[textProp] =
    select.options[select.selectedIndex].text;
  destroyCode(el);
  createCode(el, replace);
}
var forms = document.querySelectorAll('rpc-form');
for (var i = 0; i < forms.length; i++) createCode(forms[i]);