# CCRPC Forms
Embedded web forms for the Champaign County Regional Planning Commission
based on [Form.io](https://github.com/formio/formio.js/). To see the library
in action, visit [the examples page](https://ccrpc.gitlab.io/ccrpc-forms/).

## Documentation

### Components
* [`rpc-form`](src/components/rpc-form)

## Credits
CCRPC Charts was developed by Matt Yoder for the
[Champaign County Regional Planning Commission](https://ccrpc.org/).

## License
CCRPC Forms is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/ccrpc-forms/blob/master/LICENSE.md).
